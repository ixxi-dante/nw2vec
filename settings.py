import os


DATA_PATH = 'data'
BEHAVIOUR_PATH = os.path.join(DATA_PATH, 'behaviour')
SCALE_PATH = os.path.join(DATA_PATH, 'scale')
